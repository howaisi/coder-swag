//
//  DataService.swift
//  Swag-T
//
//  Created by Hozaifa on 12/21/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import Foundation
class DataService {
    static let instance = DataService()
    
    private let catagories = [Catagory(titel:"SHIRTS",imageName:"shirts.png"),Catagory(titel:"HOODIES",imageName:"hoodies.png"),
                              Catagory(titel:"HATS",imageName:"hats.png"),Catagory(titel:"DIGITAL",imageName:"digital.png")]
    
    private let hats = [Product(title:"Devslopes logo hat Benine", price: "$18", imageName: "hat01.png"),
                        Product(title:"Devslopes Logo hat Black", price: "$20", imageName: "hat02.png"),
                        Product(title:"Devslopes Logo hat White", price: "$20", imageName: "hat03.png"),
                        Product(title:"Devslopes Logo Snap Back", price: "$29", imageName: "hat04.png")]
    
    private let hoodies = [Product(title:"Devslopes logo hoodies Gray", price: "$18", imageName: "hoodie01.png"),
                           Product(title:"Devslopes Logo hoodies Red", price: "$20", imageName: "hoodie02.png"),
                           Product(title:"Devslopes  hoodies Gray", price: "$20", imageName: "hoodie03.png"),
                           Product(title:"Devslopes  hoodies Black", price: "$29", imageName: "hoodie04.png")]
    
    private let shirts = [Product(title: "Devslopes Logo Shirt Black", price: "$59", imageName: "shirt01"),
                          Product(title: "Devslopes Badge Shirt Grey", price: "$62", imageName: "shirt02"),
                          Product(title: "Devslopes Logo Shirt Red", price: "$88", imageName: "shirt03"),
                          Product(title: "Hustle Delegate Shirt Grey", price: "$39", imageName: "shirt04"),
                          Product(title: "KickFlip Studios Shirt Black", price: "$50", imageName: "shirt05")
    ]
    
    private let digitalGoods = [Product]()
    
    
    func getCategories() -> [Catagory]{
        return catagories
        
    }
    
    func getProduct(forCatagoryTitel titel:String) -> [Product]{
        switch titel {
        case "SHIRTS":
            return getShirts()
        case "HATS":
            return getHats()
        case "HOODIES":
            return getHoodies()
        case "DIGITAL":
            return getDigitalGoods()
            
        default:
            return getShirts()
        }
    }
    
    func getHats() -> [Product] {
        return hats
    }
    
    func getHoodies() -> [Product] {
        return hoodies
    }
    
    func getShirts() -> [Product] {
        return shirts
    }
    
    func getDigitalGoods() -> [Product] {
        return digitalGoods
    }
    
    
    
    
    
    
    
    
    
}

