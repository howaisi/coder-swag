//
//  CategoryCell.swift
//  Swag-T
//
//  Created by Hozaifa on 12/21/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var catagoryImage:UIImageView!
    @IBOutlet weak var catagoryTitel:UILabel!
    
    
    func updateView(catagory:Catagory) {
        catagoryImage.image = UIImage(named: catagory.imageName)
        catagoryTitel.text = catagory.title 
    }


}
