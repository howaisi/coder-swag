//
//  ProductCell.swift
//  Swag-T
//
//  Created by Hozaifa on 12/22/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitel: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    func updateViews(product: Product) {
        productImage.image = UIImage(named: product.imageName)
        productTitel.text = product.titel
        productPrice.text = product.price
    }
    
}
