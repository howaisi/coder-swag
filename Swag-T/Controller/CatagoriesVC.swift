//
//  ViewController.swift
//  Swag-T
//
//  Created by Hozaifa on 12/21/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import UIKit

class CatagoriesVC: UIViewController {

    @IBOutlet weak var catagoryTable:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        catagoryTable.delegate = self
        catagoryTable.dataSource = self
    }

    


}

extension CatagoriesVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getCategories().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "catagorycell") as? CategoryCell {
            let catagory = DataService.instance.getCategories()[indexPath.row]
            cell.updateView(catagory: catagory)
            return cell
        }else {
            return CategoryCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let catagorie = DataService.instance.getCategories()[indexPath.row]
        performSegue(withIdentifier: "ProductsVC", sender: catagorie)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ProductsVC = segue.destination as? ProductsVC {
            let barButton = UIBarButtonItem()
            barButton.title = " "
            navigationItem.backBarButtonItem = barButton 
            assert(sender as? Catagory != nil)
            ProductsVC.initProducts(catagory: sender as! Catagory)
           
        }
        
    }
}
