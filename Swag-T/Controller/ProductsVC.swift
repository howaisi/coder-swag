//
//  ProductsVC.swift
//  Swag-T
//
//  Created by Hozaifa on 12/22/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import UIKit

class ProductsVC: UIViewController {
    
    @IBOutlet weak var ProductsOfCollection: UICollectionView!
    private(set) public var products = [Product]()
    override func viewDidLoad() {
        super.viewDidLoad()
        ProductsOfCollection.delegate = self
        ProductsOfCollection.dataSource = self 
    }
    func initProducts(catagory:Catagory) {
         products = DataService.instance.getProduct(forCatagoryTitel: catagory.title)
        navigationItem.title = catagory.title 
    }

}

extension ProductsVC: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath)as? ProductCell {
            let product = products[indexPath.row]
            cell.updateViews(product: product)
            return cell
        }
        return ProductCell() 
    }
    
    
}
