//
//  Product.swift
//  Swag-T
//
//  Created by Hozaifa on 12/22/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import Foundation
struct Product {
    private (set) public var titel: String
    private (set) public var price:String
    private (set) public var imageName: String
    
    init(title:String,price:String,imageName:String) {
        self.titel = title
        self.imageName = imageName
        self.price = price
    }
}
