//
//  Catagory.swift
//  Swag-T
//
//  Created by Hozaifa on 12/21/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import Foundation
struct Catagory {
    private (set) public var title: String
    private (set) public var imageName: String
    
    init(titel:String, imageName:String) {
        self.title = titel
        self.imageName = imageName
    }
}
